<?php $__env->startSection('content'); ?>
    <h1 class="mt-3">List</h1>
    <?php if(count($shortens)>0): ?>
        <?php $__currentLoopData = $shortens; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $shorten): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
            <div>
                <p><?php echo e($shorten->created_at); ?></p>
                <a href="<?php echo e(url($shorten->long)); ?>">
                    <h3 class="text-warning"><?php echo e($shorten->long); ?></h3>
                </a>
                <input id="shorturl<?php echo e($shorten->id); ?>" class="form-controller" type="text"
                       value="http://www.shorturl.local/t/<?php echo e($shorten->short); ?>" readonly>
                <button onclick="copy(this)" value="<?php echo e($shorten->id); ?>" type="button" class="btn btn-primary">copy</button>
                <p><?php echo e($shorten->view); ?></p>
            </div>
            <hr>
        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
    <?php endif; ?>

    <script>
        function copy(clickedBtn){
            var id = clickedBtn.value;
            var copyText = document.querySelector('#shorturl'+id);
            copyText.select();
            document.execCommand('copy');
            alert('Copied '+ copyText.value);
        }
    </script>

<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\xampp\htdocs\shorturl\resources\views/index.blade.php ENDPATH**/ ?>